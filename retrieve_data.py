from google_play_scraper import Sort, reviews_all
import pandas as pd 
import numpy as np 

languages = ['ar','es','fr','hi','it''pt','ru','us','zh'] 
general = []

for language in languages:

    result = reviews_all(
        'com.halloglobal.flutterapp.hallo',
        #sleep_milliseconds=0, # defaults to 0
        lang=language, # defaults to 'en'
        country='us', # defaults to 'us'
        sort=Sort.NEWEST, # defaults to Sort.MOST_RELEVANT
        #filter_score_with=5 # defaults to None(means all score)
    )

    items = []

    for element in result:
        content = element['content']
        score = int(element['score'])
        thumbs = int(element['thumbsUpCount'])

        items.append([content,score,thumbs])
        general.append([content,score,thumbs])

    df = pd.DataFrame(np.array(items), columns=['content','score','thumbs'])
    df['score'] = pd.to_numeric(df['score'])
    df['thumbs'] = pd.to_numeric(df['thumbs'])
    df = df[df['score'] != 3]
    df['Positively Rated'] = np.where(df['score'] > 3, 1, 0)
    path = './data/data_' + language + '.csv'
    df.to_csv(path)
    
    
df_general = pd.DataFrame(np.array(general), columns=['content','score','thumbs'])
df_general['score'] = pd.to_numeric(df_general['score'])
df_general['thumbs'] = pd.to_numeric(df_general['thumbs'])
df_general = df_general[df_general['score'] != 3]
df_general['Positively Rated'] = np.where(df_general['score'] > 3, 1, 0)
path = './data/data_general.csv'
df_general.to_csv(path)