from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from langdetect import detect
import pandas as pd 
import numpy as np 
import re

#languages = ['ar','es','fr','hi','it''pt','ru','us','zh'] 

def detect_language(text):
    return detect(text)

def encode_emojis(text):
    return text.encode('unicode-escape')

def decode_emojis(text):
    return text.decode("utf-8")


a = """

df = pd.read_csv('./data/data_general.csv')

df['content'] = df['content'].apply(encode_emojis)
df['content'] = df['content'].apply(decode_emojis)
#df['content'] = df['content'].str.replace(r'\\[Uu]\S*','') # eliminar emojis
df['content'] = df['content'].str.replace(r'\\.\S*','') # eliminar emojis
df = df[df['content'].str.contains('[A-Za-z]', na=False)] 
df['language'] = df['content'].apply(detect_language)

#df.to_csv('./data/data_general_final.csv')

result = [x for _, x in df.groupby(df['language'])] 

english_list = ['en', 'af', 'ca', 'cs', 'cy', 'da', 'de', 'et', 'fi', 'hr', 'hu', 'id', 'it', 'nl', 'no', 'pl', 'ro', 'sl', 'so', 'sq', 'sv', 'sw', 'tl' ]
# descartado sk lv tr vi pt es

for i in result:
    if i['language'].iloc[0] in english_list:
        i.to_csv('./data/final/en.csv',mode='a')

#"""


data = pd.read_csv('./data/final/en.csv')
data.dropna(inplace=True)

# Split data into train and test sets.
X_train, X_test, y_train, y_test = train_test_split(data['content'], data['Positively Rated'], random_state=0)

# COUNT VECTORIZER
# Fit the CountVectorizer to the training data specifiying a minimum document frequency of 5 and extracting 1-grams and 2-grams
vect = CountVectorizer(min_df=3, ngram_range=(1,2)).fit(X_train)

# len(vect.get_feature_names()) # Este es el tamaño de las caracteristicas que determinarán el target si es 0 o 1. VECT.get_feature_names() es el vocabulario

# transform the documents in the training data to a document-term matrix
X_train_vectorized = vect.transform(X_train)

model = []
model = LogisticRegression()
model.fit(X_train_vectorized, y_train)
predictions = model.predict(vect.transform(X_test))

print('AUC: ',roc_auc_score(y_test, predictions))

comments = ['it is a bad app', 'great app',"not good for me","the worst app in the world"]

print(model.predict(vect.transform(comments)))


